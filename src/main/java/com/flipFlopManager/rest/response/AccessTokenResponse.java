package com.flipFlopManager.rest.response;

public class AccessTokenResponse {

	private String token;

	public AccessTokenResponse(String accessToken) {
		this.token = accessToken;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
