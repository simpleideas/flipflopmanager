package com.flipFlopManager.rest.response;

public class HelpRequestCardView {

	private String cardId;
	private String description;
	private String username;
	private String avatarUrl;
	private String sinceHours;
	private boolean owner;

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getSinceHours() {
		return sinceHours;
	}

	public void setSinceHours(String sinceHours) {
		this.sinceHours = sinceHours;
	}
	
	public boolean isOwner() {
		return owner;
	}

	public void setOwner(boolean owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "HelpRequestCardView [cardId=" + cardId + ", description="
				+ description + ", username=" + username + ", avatarUrl="
				+ avatarUrl + ", sinceHours=" + sinceHours + ", owner=" + owner
				+ "]";
	}

	
}
