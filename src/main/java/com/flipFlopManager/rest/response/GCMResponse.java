package com.flipFlopManager.rest.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GCMResponse {
	@JsonProperty("message_id")
	private String messageId;

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	@Override
	public String toString() {
		return "GCMResponse [messageId=" + messageId + "]";
	}
	
}
