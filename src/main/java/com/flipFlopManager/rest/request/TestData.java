package com.flipFlopManager.rest.request;

import com.flipFlopManager.model.PlayerStats;

public class TestData {

	private String message;
	private PlayerStats playerStats;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public PlayerStats getPlayerStats() {
		return playerStats;
	}

	public void setPlayerStats(PlayerStats playerStats) {
		this.playerStats = playerStats;
	}
}
