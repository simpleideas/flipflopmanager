package com.flipFlopManager.rest.request;

public class HelpRequestDTO {

	private String cardId;
	private String googleId;

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getGoogleId() {
		return googleId;
	}

	public void setGoogleId(String googleId) {
		this.googleId = googleId;
	}

	@Override
	public String toString() {
		return "HelpRequestDTO [cardId=" + cardId + ", googleId=" + googleId
				+ "]";
	}

}