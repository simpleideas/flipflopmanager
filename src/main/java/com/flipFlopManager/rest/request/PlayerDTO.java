package com.flipFlopManager.rest.request;

import java.io.Serializable;

import com.flipFlopManager.model.Role;

public class PlayerDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String googleEmail;
	private String accessToken;
	private String boardId;
	private Role role;
	private String gcmRegistrationToken;

	public String getGoogleEmail() {
		return googleEmail;
	}

	public void setGoogleEmail(String googleEmail) {
		this.googleEmail = googleEmail;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

	public String getGcmRegistrationToken() {
		return gcmRegistrationToken;
	}

	public void setGcmRegistrationToken(String gcmRegistrationToken) {
		this.gcmRegistrationToken = gcmRegistrationToken;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
}
