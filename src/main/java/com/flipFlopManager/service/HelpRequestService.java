package com.flipFlopManager.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flipFlopManager.model.HelpRequest;
import com.flipFlopManager.model.HelpRequestStatus;
import com.flipFlopManager.model.Player;
import com.flipFlopManager.repository.GamesRepository;
import com.flipFlopManager.repository.HelpRequestRepository;
import com.flipFlopManager.repository.PlayerRepository;
import com.flipFlopManager.rest.request.HelpRequestDTO;
import com.flipFlopManager.rest.response.HelpRequestCardView;
import com.flipFlopManager.transformer.HelpRequestToCardViewTransformer;
import com.google.common.collect.Lists;
import com.julienvey.trello.Trello;

@Service
public class HelpRequestService {

	private Trello trelloClient;
	private HelpRequestRepository helpRequestRepository;
	private PlayerRepository playerRepository;
	private HelpRequestToCardViewTransformer helpRequestToCardViewTransformer;
	private GCMService gcmService;
	private PlayerBuilder playerBuilder;

	@Autowired
	public HelpRequestService(HelpRequestRepository helpRequestRepository,
			Trello trelloClient, PlayerRepository playerRepository,
			GamesRepository gamesRepository,
			HelpRequestToCardViewTransformer helpRequestToCardViewTransformer,GCMService gcmService,PlayerBuilder playerBuilder) {
		this.helpRequestRepository = helpRequestRepository;
		this.trelloClient = trelloClient;
		this.playerRepository = playerRepository;
		this.helpRequestToCardViewTransformer = helpRequestToCardViewTransformer;
		this.gcmService = gcmService;
		this.playerBuilder = playerBuilder;
	}

	public List<HelpRequestCardView> getOpenHelpRequests(Player player) {
		List<HelpRequest> helpRequests = this.helpRequestRepository
				.findAllByStatusAndBoardId(HelpRequestStatus.OPEN, player.getBoardId());
		List<HelpRequestCardView> helpRequestCardViews = Lists
				.newArrayListWithCapacity(helpRequests.size());
		for (HelpRequest helpRequest : helpRequests) {
			helpRequestCardViews.add(this.helpRequestToCardViewTransformer
					.apply(helpRequest,player));
		}
		return helpRequestCardViews;
	}

	public HelpRequest closeHelpRequest(HelpRequestDTO helpRequestDTO) {
		HelpRequest helpRequest = this.helpRequestRepository
				.findOneByCardId(helpRequestDTO.getCardId());

		if (HelpRequestStatus.OPEN.equals(helpRequest.getStatus())) {
			Player helper = this.playerRepository
					.findOneByGoogleEmail(helpRequestDTO.getGoogleId());
			Player requester = this.playerRepository
					.findOneByGoogleEmail(helpRequest.getRequesterId());

			if (helper != null && requester != null) {
				helpRequest.setHelperId(helpRequestDTO.getGoogleId());
				helpRequest.setStatus(HelpRequestStatus.CLOSE);
				this.trelloClient.addMemberToCard(helpRequest.getCardId(),
						helper.getTrelloId(), helper.getAccessToken());
				this.helpRequestRepository.save(helpRequest);
				this.playerBuilder.addTrelloInfoToPlayer(helper);
				this.gcmService.sendMessage(helper.getMember().getUsername() + " te está ayudando!", requester.getGcmRegistrationToken(),null);
			}
		}
		return helpRequest;
	}

	public HelpRequest openHelpRequest(HelpRequestDTO helpRequestDTO) {
		Player requester = this.playerRepository
				.findOneByGoogleEmail(helpRequestDTO.getGoogleId());
		HelpRequest helpRequest = this.helpRequestRepository
				.findOneByCardId(helpRequestDTO.getCardId());
		if (helpRequest == null) {
			helpRequest = new HelpRequest();
			helpRequest.setCardId(helpRequestDTO.getCardId());
			helpRequest.setRequesterId(helpRequestDTO.getGoogleId());
			helpRequest.setCreationDate(new Date());
			helpRequest.setStatus(HelpRequestStatus.OPEN);
			helpRequest.setBoardId(requester.getBoardId());
			this.helpRequestRepository.save(helpRequest);
			
			decrementAvailableHelpRequest(requester);
			
			this.playerBuilder.addTrelloInfoToPlayer(requester);
			this.gcmService.sendMessage(requester.getMember().getUsername() + " necesita ayuda para resolver una tarea!");
		}
		return helpRequest;
	}

	
	private void decrementAvailableHelpRequest(Player requester) {
		if (requester.getInitialHelpRequests() > 0){
			requester.setInitialHelpRequests(requester.getInitialHelpRequests() - 1);
		}else{
			requester.setAvailableHelpRequests(requester.getAvailableHelpRequests() - 1);
		}
		this.playerRepository.save(requester);
	}

	public HelpRequest deleteHelpRequest(String cardId) {
		HelpRequest helpRequest = this.helpRequestRepository
				.findOneByCardId(cardId);
		if(helpRequest != null && HelpRequestStatus.OPEN.equals(helpRequest.getStatus())){
			this.helpRequestRepository.delete(helpRequest);
			return helpRequest;
		}
		return null;
	}
	

	

}