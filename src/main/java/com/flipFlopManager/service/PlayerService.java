package com.flipFlopManager.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipFlopManager.model.CardInfo;
import com.flipFlopManager.model.Game;
import com.flipFlopManager.model.HelpRequest;
import com.flipFlopManager.model.Player;
import com.flipFlopManager.model.PlayerStats;
import com.flipFlopManager.model.Role;
import com.flipFlopManager.model.Skill;
import com.flipFlopManager.model.SkillName;
import com.flipFlopManager.repository.CardsDao;
import com.flipFlopManager.repository.CardsRepository;
import com.flipFlopManager.repository.PlayerRepository;
import com.flipFlopManager.rest.request.PlayerDTO;
import com.flipFlopManager.rest.response.AccessTokenResponse;
import com.flipFlopManager.transformer.MemberToSimpleMemberTransformer;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.julienvey.trello.Trello;
import com.julienvey.trello.domain.Board;
import com.julienvey.trello.domain.Card;
import com.julienvey.trello.domain.Member;

@Service
public class PlayerService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(PlayerService.class);

	private Trello trelloClient;
	private MemberToSimpleMemberTransformer memberToPlayerTransformer;
	private PlayerRepository playerRepository;
	private GameService gameService;
	private HelpRequestService helpRequestService;
	
	@Autowired
	private PlayerBuilder playerBuilder;

	@Autowired
	public PlayerService(Trello trelloClient,
			MemberToSimpleMemberTransformer memberToPlayerTransformer,
			PlayerRepository playerRepository, GameService gameService,
			HelpRequestService helpRequestService,CardsRepository cardsRepository) {
		this.trelloClient = trelloClient;
		this.memberToPlayerTransformer = memberToPlayerTransformer;
		this.playerRepository = playerRepository;
		this.gameService = gameService;
		this.helpRequestService = helpRequestService;
	}

	public List<Board> getBoards(String token) {
		return this.trelloClient.getMemberBoards(token);
	}

	public Player create(PlayerDTO playerDTO) {
		Member member = this.trelloClient.getMemberInformationByToken(playerDTO
				.getAccessToken());
		Player player = this.playerRepository.findOneByAccessToken(playerDTO
				.getAccessToken());
		if (player == null) {
			player = new Player();
			player.setAccessToken(playerDTO.getAccessToken());
			player.setGoogleEmail(playerDTO.getGoogleEmail());
			player.setGcmRegistrationToken(playerDTO.getGcmRegistrationToken());
			player.setTrelloId(member.getId());
			this.playerRepository.save(player);
		}
		addTrelloInfoToPlayer(member, player);
		return player;
	}

	public List<Card> getCards(String token) {
		Player player = this.playerRepository.findOneByAccessToken(token);
		return this.trelloClient.getBoardMemberCards(player.getBoardId(),
				player.getTrelloId(), token);
	}

	public Player getPlayerByToken(String token) {
		Player player = this.playerRepository.findOneByAccessToken(token);
		playerBuilder.addInfoToPlayer(player);
		player.setHelpRequests(this.helpRequestService.getOpenHelpRequests(player));
		return player;
	}
	
	private void addTrelloInfoToPlayer(Member member, Player player) {
		player.setMember(this.memberToPlayerTransformer.apply(member));
	}

	public AccessTokenResponse getPlayerToken(String googleEmail) {
		Player player = this.playerRepository.findOneByGoogleEmail(googleEmail);
		if (player != null) {
			return new AccessTokenResponse(player.getAccessToken());
		}
		return null;
	}

	//patch prueba
	public Player update(String token, JsonPatch jsonPatch) throws IOException,
			JsonPatchException {
		Player playerToUpdate = this.playerRepository
				.findOneByAccessToken(token);
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(playerToUpdate);
		JsonNode playerNode = mapper.readTree(json);
		JsonNode playerUpdatedNode = jsonPatch.apply(playerNode);
		Player playerUpdated = mapper.treeToValue(playerUpdatedNode,
				Player.class);
		this.playerRepository.save(playerUpdated);
		return playerUpdated;
	}

	public Player updateBoardId(String token, String boardId) {
		Player player = this.playerRepository.findOneByAccessToken(token);
		player.setBoardId(boardId);
		this.playerRepository.save(player);
		return player;
	}
	
	public Player updateGcmRegistrationToken(String token, String gcmRegistrationToken) {
		Player player = this.playerRepository.findOneByAccessToken(token);
		player.setGcmRegistrationToken(gcmRegistrationToken);
		this.playerRepository.save(player);
		return player;
	}

	public Player updateRole(String token, Role role) {
		Player player = this.playerRepository.findOneByAccessToken(token);
		player.setRole(role);
		this.playerRepository.save(player);
		playerBuilder.addInfoToPlayer(player);
		return player;
	}

	private void addCardsToPlayer(Player player, Game game) {
		List<Card> cards = this.trelloClient.getBoardMemberCards(
				player.getBoardId(), player.getTrelloId(),
				player.getAccessToken());

		for (Card card : cards) {
			String idList = card.getIdList();
			if (idList.equals(game.getTodoListId())) {
				player.getTodoTasks().add(card);
			}
			if (idList.equals(game.getInProgressListId())) {
				player.getInProgressTasks().add(card);
			}
			if (idList.equals(game.getDoneListId())) {
				player.getDoneTasks().add(card);
			}
			player.getAllTasks().add(card);
		}
	}

	public Player updateRoleAndBoard(String token, PlayerDTO playerDTO) {
		Player player = this.playerRepository.findOneByAccessToken(token);
		player.setBoardId(playerDTO.getBoardId());
		player.setRole(playerDTO.getRole());
		this.playerRepository.save(player);
		this.gameService.createNewGame(playerDTO.getBoardId(), player.getAccessToken());
		playerBuilder.addInfoToPlayer(player);
		return player;
	}

	public List<Player> getPlayers() {
		List<Player> players = this.playerRepository.findAll();
		for (Player player : players) {
			playerBuilder.addInfoToPlayer(player);
		}
		return players;
	}

	public PlayerStats getLastPlayerStats(String token) {
		Player player = this.playerRepository.findOneByAccessToken(token);
		Game game = this.gameService.getGame(player.getBoardId());
		this.playerBuilder.addPlayerStats(player, game.getWeekNumber() - 1);
		return player.getStats();
	}

	

}