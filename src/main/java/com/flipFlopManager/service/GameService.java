package com.flipFlopManager.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flipFlopManager.model.Constants;
import com.flipFlopManager.model.Game;
import com.flipFlopManager.repository.GamesRepository;
import com.julienvey.trello.Trello;
import com.julienvey.trello.domain.TList;

@Service
public class GameService {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(GameService.class);
	private GamesRepository gamesRepository;
	private Trello trello;

	@Autowired
	public GameService(GamesRepository gamesRepository, Trello trello) {
		this.gamesRepository = gamesRepository;
		this.trello = trello;
	}

	public Game createNewGame(String boardId, String token) {
		if (!this.gamesRepository.exists(boardId)) {
			LOGGER.info("Creating new game with id : {}", boardId);
			Game game = new Game();
			game.setBoardId(boardId);
			List<TList> boardLists = this.trello.getBoardLists(boardId, token);
			for (TList list : boardLists) {
				switch (list.getName()) {
				case Constants.TO_DO:
					game.setTodoListId(list.getId());
					break;
				case Constants.IN_PROGRESS:
					game.setInProgressListId(list.getId());
					break;
				case Constants.DONE:
					game.setDoneListId(list.getId());
					break;
				default:
					break;
				}
			}
			this.gamesRepository.save(game);
			LOGGER.info("New game successfully created: {}", game);
			return game;
		} else {
			LOGGER.info("Already exist game with id: {}", boardId);
			return this.gamesRepository.findOne(boardId);
		}
	}

	public Game getGame(String boardId) {
		return this.gamesRepository.findOne(boardId);
	}
}
