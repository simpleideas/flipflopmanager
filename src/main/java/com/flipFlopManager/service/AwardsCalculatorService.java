package com.flipFlopManager.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.flipFlopManager.model.CardInfo;
import com.flipFlopManager.model.Constants;
import com.flipFlopManager.model.Game;
import com.flipFlopManager.model.HelpRequest;
import com.flipFlopManager.model.Player;
import com.flipFlopManager.repository.CardsRepository;
import com.flipFlopManager.repository.GamesRepository;
import com.flipFlopManager.repository.HelpRequestRepository;
import com.flipFlopManager.repository.PlayerRepository;
import com.flipFlopManager.transformer.CardInfoBuilder;
import com.julienvey.trello.Trello;
import com.julienvey.trello.domain.Card;

@Service
public class AwardsCalculatorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AwardsCalculatorService.class);

	private Trello trello;
	private PlayerRepository playerRepository;
	private CardsRepository cardsRepository;
	private GamesRepository gamesRepository;
	private HelpRequestRepository helpRequestRepository;
	private CardInfoBuilder cardInfoBuilder;
	private GCMService gcmService;
	@Autowired
	private PlayerBuilder playerBuilder;

	@Autowired
	public AwardsCalculatorService(Trello trelloClient, PlayerRepository playerRepository,
			CardsRepository cardsRepository, GamesRepository gamesRepository,
			HelpRequestRepository helpRequestRepository, CardInfoBuilder cardInfoBuilder, GCMService gcmService) {
		this.trello = trelloClient;
		this.playerRepository = playerRepository;
		this.cardsRepository = cardsRepository;
		this.gamesRepository = gamesRepository;
		this.helpRequestRepository = helpRequestRepository;
		this.cardInfoBuilder = cardInfoBuilder;
		this.gcmService = gcmService;

	}
	
	/**
	 * Todos los viernes a las 18:00 hs se realiza la entrega de logros.
	 */
	@Scheduled(cron = "0 0 18 * * FRI")
	public void calculatePoints() {
		LOGGER.info("Point calculation STARTED");
		List<Player> players = this.playerRepository.findAll();
		List<Card> playerCards;
		Game playerGame;
		int cardsDocumentedTotal;
		for (Player player : players) {
			try {
				LOGGER.info("Player: {}", player.getAccessToken());
				cardsDocumentedTotal = 0;
				// TODO pedir las card de una lista especifica
				playerCards = this.trello.getBoardMemberCards(player.getBoardId(), player.getTrelloId(),
						player.getAccessToken());
				playerGame = this.gamesRepository.findOne(player.getBoardId());
				for (Card card : playerCards) {
					if (card.getIdList().equals(playerGame.getDoneListId())) {
						LOGGER.info("Card '{}' is in done list!", card.getName());
						CardInfo cardInfo = calculateCardPoints(player, card, playerGame.getWeekNumber());
						if (cardInfo != null && cardInfo.isDocumented()){
							cardsDocumentedTotal++;
						}
					}
				}
				incrementAvailableHelpRequest(player,cardsDocumentedTotal);
				
			} catch (Exception e) {
				LOGGER.error("Se ignora el player {}", player.getAccessToken(), e);
			}
		}

		// busco ganador y perdedor de cada juego
		List<Game> allGames = this.gamesRepository.findAll();
		List<Player> playersByGame;
		List<CardInfo> weekCards;
		List<HelpRequest> weekHelpRequest;
		Integer weekPoints;
		Integer maxPoints = Integer.MIN_VALUE;
		Integer minPoints = Integer.MAX_VALUE;
		Player winner = null;
		Player loser = null;
		for (Game game : allGames) {
			maxPoints = Integer.MIN_VALUE;
			minPoints = Integer.MAX_VALUE;
			playersByGame = this.playerRepository.findAllByBoardId(game.getBoardId());
			for (Player player : playersByGame) {
				weekPoints = 0;
				weekCards = this.cardsRepository.findAllByPlayerIdAndWeekNumber(player.getGoogleEmail(),
						game.getWeekNumber());
				weekHelpRequest = this.helpRequestRepository.findAllByHelperIdAndWeekNumber(player.getGoogleEmail(),
						game.getWeekNumber());
				for (HelpRequest helpRequest : weekHelpRequest) {
					weekPoints = weekPoints + helpRequest.getPoints();
				}
				for (CardInfo cardInfo : weekCards) {
					weekPoints = weekPoints + cardInfo.getPoints();
				}
				if (weekPoints > maxPoints) {
					maxPoints = weekPoints;
					winner = player;
				}
				if (weekPoints < minPoints) {
					minPoints = weekPoints;
					loser = player;
				}
			}
			
			//update winner count
			winner.setTotalGamesWon(winner.getTotalGamesWon() + 1);
			this.playerRepository.save(winner);
			
			//mensajes
			playersByGame = this.playerRepository.findAllByBoardId(game.getBoardId());
			for (Player player : playersByGame) {
				String playerId = player.getGoogleEmail();
				if (playerId.equals(loser.getGoogleEmail())) {
					this.sendNotification(player, Constants.POINTS_CALCULATION_MESSAGE_LOSER);
				} else if (playerId.equals(winner.getGoogleEmail())) {
					this.sendNotification(player, Constants.POINTS_CALCULATION_MESSAGE_WINNER);
				} else {
					this.sendNotification(player, Constants.POINTS_CALCULATION_MESSAGE_GENERIC);
				}
			}
		}
		incrementGamesWeekNumber();
		LOGGER.info("Point calculation COMPLETED");
	}
	
	private void incrementAvailableHelpRequest(Player player, int cardsDocumentedTotal) {
		int totalDocumented = player.getDocumentedCardsCounter() + cardsDocumentedTotal;
		player.setAvailableHelpRequests(player.getAvailableHelpRequests() + (totalDocumented / 5));
		player.setDocumentedCardsCounter(totalDocumented % 5);
		this.playerRepository.save(player);
	}

	private void sendNotification(Player player, String message){
		this.gcmService.sendMessage(message,
				player.getGcmRegistrationToken(), this.playerBuilder.getPlayerStats(player));
	}

	private void incrementGamesWeekNumber() {
		List<Game> games = this.gamesRepository.findAll();
		for (Game game : games) {
			game.setWeekNumber(game.getWeekNumber() + 1);
			this.gamesRepository.save(game);
		}
	}

	private CardInfo calculateCardPoints(Player player, Card card, int weekNumber) {
		if (this.checkOwner(player, card)) {
			CardInfo cardInfo = this.cardInfoBuilder.buildCardInfo(card, player, weekNumber);
			this.cardsRepository.save(cardInfo);
			return cardInfo;
		}
		return null;
	}

	public boolean checkOwner(Player player, Card card) {
		if (card.getIdMembers().size() > 1) {
			HelpRequest helpRequest = this.helpRequestRepository.findOneByCardId(card.getId());
			if (helpRequest != null) {
				return helpRequest.getRequesterId().equals(player.getGoogleEmail());
			}
		}
		return true;
	}

}