package com.flipFlopManager.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.flipFlopManager.model.CardInfo;
import com.flipFlopManager.model.Game;
import com.flipFlopManager.model.HelpRequest;
import com.flipFlopManager.model.HelpRequestStatus;
import com.flipFlopManager.model.Player;
import com.flipFlopManager.model.PlayerStats;
import com.flipFlopManager.model.Skill;
import com.flipFlopManager.model.SkillName;
import com.flipFlopManager.repository.CardsRepository;
import com.flipFlopManager.repository.HelpRequestRepository;
import com.flipFlopManager.repository.PlayerRepository;
import com.flipFlopManager.transformer.MemberToSimpleMemberTransformer;
import com.julienvey.trello.Trello;
import com.julienvey.trello.domain.Card;
import com.julienvey.trello.domain.Member;

@Component
public class PlayerBuilder {

	@Autowired
	private Trello trelloClient;
	@Autowired
	private MemberToSimpleMemberTransformer memberToPlayerTransformer;
	@Autowired
	private GameService gameService;
	@Autowired
	private CardsRepository cardsRepository;
	@Autowired
	private HelpRequestRepository helpRequestRepository;
	@Autowired
	private PlayerRepository playerRepository;

	public void addInfoToPlayer(Player player) {

		addTrelloInfoToPlayer(player);
		Game game = this.gameService.getGame(player.getBoardId());
		addCardsToPlayer(player, game);
		addPlayerStats(player, null);
	}

	public void addTrelloInfoToPlayer(Player player) {
		Member member = this.trelloClient.getMemberInformationByToken(player.getAccessToken());
		player.setMember(this.memberToPlayerTransformer.apply(member));
	}

	private void addCardsToPlayer(Player player, Game game) {
		List<Card> cards = this.trelloClient.getBoardMemberCards(player.getBoardId(), player.getTrelloId(),
				player.getAccessToken());

		for (Card card : cards) {
			card.setOwner(this.checkOwner(player, card));
			String idList = card.getIdList();
			if (idList.equals(game.getTodoListId())) {
				player.getTodoTasks().add(card);
			}
			if (idList.equals(game.getInProgressListId())) {
				player.getInProgressTasks().add(card);
			}
			if (idList.equals(game.getDoneListId())) {
				player.getDoneTasks().add(card);
			}
			player.getAllTasks().add(card);
		}
	}

	public void addPlayerStats(Player player, Integer weekNumber) {
		List<CardInfo> cards = getPlayerCards(player, weekNumber);
		PlayerStats playerStats = getPlayerStats(player, cards);
		createPlayerSkills(player, playerStats);
	}

	public PlayerStats getPlayerStats(Player player) {
		List<CardInfo> cards = getPlayerCards(player, null);
		return getPlayerStats(player, cards);
	}

	private List<CardInfo> getPlayerCards(Player player, Integer weekNumber) {
		List<CardInfo> cards;
		if (weekNumber == null) {
			cards = this.cardsRepository.findAllByPlayerId(player.getGoogleEmail());
		} else {
			cards = this.cardsRepository.findAllByPlayerIdAndWeekNumber(player.getGoogleEmail(), weekNumber);
		}
		return cards;
	}

	private void createPlayerSkills(Player player, PlayerStats playerStats) {
		int totalCards = playerStats.getTotalCards();

		int documentador = getPercentage(playerStats.getTotalDocumented(),
				totalCards);
		player.getSkills().add(new Skill(SkillName.DOCUMENTADOR, documentador));

		int versatilidad = getPercentage(playerStats.getTotalOtherRoles(),
				totalCards);
		player.getSkills().add(new Skill(SkillName.VERSATILIDAD, versatilidad));

		int puntualidad = getPercentage(playerStats.getTotalOnTime(),
				totalCards);
		player.getSkills().add(new Skill(SkillName.PUNTUALIDAD, puntualidad));

		int independencia = getPercentage(playerStats.getTotalWithoutHelp(),
				totalCards);
		player.getSkills().add(
				new Skill(SkillName.INDEPENDENCIA, independencia));

		List<HelpRequest> helpRequests = this.helpRequestRepository.findAllByHelperId(
				player.getGoogleEmail());
		int totalHelper = helpRequests.size();
		playerStats.setTotalHelper(totalHelper);
		for (HelpRequest helpRequest : helpRequests){
			playerStats.setTotalPoints(playerStats.getTotalPoints() + helpRequest.getPoints());
		}
		
		// info de help request
		if(player.getInitialHelpRequests() > 0){
			player.setHelpRequestEnabled(Boolean.TRUE);
		}else{
			int totalHelpRequestClosed = this.helpRequestRepository.findAllByStatusAndBoardId(HelpRequestStatus.CLOSE, player.getBoardId()).size();
			int totalPlayers = this.playerRepository.findAllByBoardId(player.getBoardId()).size();
			player.setHelpRequestEnabled(totalHelper > (totalHelpRequestClosed / (totalPlayers * 2)));
		}
		
		int totalHelpRequests = this.helpRequestRepository.findAllByBoardId(
				player.getBoardId()).size();
		
		int cooperacion = getPercentage(totalHelper, totalHelpRequests);
		player.getSkills().add(new Skill(SkillName.COOPERACION, cooperacion));

		int superacion = (int) (((double) playerStats.getSumLevel() / (double) totalCards) * 20);
		player.getSkills().add(new Skill(SkillName.SUPERACION, superacion));
	}

	private PlayerStats getPlayerStats(Player player, List<CardInfo> cards) {
		PlayerStats playerStats = new PlayerStats();
		playerStats.setTotalCards(cards.size());
		playerStats.setTotalGamesWon(player.getTotalGamesWon());
		for (CardInfo cardInfo : cards) {
			playerStats.setTotalPoints(playerStats.getTotalPoints() + cardInfo.getPoints());
			if (cardInfo.isDocumented()) {
				playerStats.setTotalDocumented(playerStats.getTotalDocumented() + 1);
			}
			if (cardInfo.isSolvedOnTime()) {
				playerStats.setTotalOnTime(playerStats.getTotalOnTime() + 1);
			}
			if (!cardInfo.getRole().equals(player.getRole())) {
				playerStats.setTotalOtherRoles(playerStats.getTotalOtherRoles() + 1);
			}
			if (cardInfo.isSolvedWithHelp()) {
				playerStats.setTotalWithHelp(playerStats.getTotalWithHelp() + 1);
			} else {
				playerStats.setTotalWithoutHelp(playerStats.getTotalWithoutHelp() + 1);
			}
			playerStats.setSumLevel(playerStats.getSumLevel() + Integer.valueOf(cardInfo.getLevel()));
		}
		player.setStats(playerStats);
		return playerStats;
	}

	private int getPercentage(int totalSkill, int totalCards) {
		return (int) (((double) totalSkill / (double) totalCards) * 100);
	}

	public boolean checkOwner(Player player, Card card) {
		if (card.getIdMembers().size() > 1) {
			HelpRequest helpRequest = this.helpRequestRepository.findOneByCardId(card.getId());
			if (helpRequest != null) {
				return helpRequest.getRequesterId().equals(player.getGoogleEmail());
			}
		}
		return true;
	}

}
