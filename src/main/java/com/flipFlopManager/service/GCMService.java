package com.flipFlopManager.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.flipFlopManager.model.PlayerStats;
import com.flipFlopManager.rest.request.GCMMessage;
import com.flipFlopManager.rest.request.TestData;
import com.flipFlopManager.rest.response.GCMResponse;

@Service
public class GCMService {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(GCMService.class);
	private static final String KEY = "AIzaSyDv5keNZALLUnAJEUGIXLscx6bc-QH7hf4";
	private static final String URL = "https://gcm-http.googleapis.com/gcm/send"; 

	public GCMResponse sendMessage(String message, String token,PlayerStats playerStats){
		LOGGER.info("Sending GCM message: {}", message);
		HttpEntity<GCMMessage> request = new HttpEntity<GCMMessage>(buildGCMMessage(message, token,playerStats),
				buildHeaders());
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		GCMResponse response = restTemplate.postForObject(URL,request, GCMResponse.class);
		LOGGER.info("GCM response: {}", response);
		return response;
	}
	
	public GCMResponse sendMessage(String message){
		return this.sendMessage(message, null, null);
	}

	private GCMMessage buildGCMMessage(String message, String token, PlayerStats playerStats) {
		GCMMessage gcmMessage = new GCMMessage();
		if (token != null){
			gcmMessage.setTo(token);
		}else{
			gcmMessage.setTo("/topics/global");
		}
		gcmMessage.setData(buildTestData(message, playerStats));
		return gcmMessage;
	}

	private TestData buildTestData(String message, PlayerStats playerStats) {
		TestData testData = new TestData();
		testData.setMessage(message);
		testData.setPlayerStats(playerStats);
		return testData;
	}

	private HttpHeaders buildHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "key=" + KEY);
		headers.add("Content-Type", "application/json");
		return headers;
	}
}
