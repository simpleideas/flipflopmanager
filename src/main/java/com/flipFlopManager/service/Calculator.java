package com.flipFlopManager.service;

import java.math.BigDecimal;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flipFlopManager.model.CardInfo;
import com.flipFlopManager.model.Player;

public class Calculator {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(Calculator.class);
	private static HashMap<String, BigDecimal> levelPointMap = new HashMap<>();
	private static String LEVEL_1 = "1";
	private static String LEVEL_2 = "2";
	private static String LEVEL_3 = "3";
	private static String LEVEL_4 = "4";
	private static String LEVEL_5 = "5";
	private static BigDecimal POINTS_LEVEL_1 = new BigDecimal("10");
	private static BigDecimal POINTS_LEVEL_2 = new BigDecimal("20");
	private static BigDecimal POINTS_LEVEL_3 = new BigDecimal("30");
	private static BigDecimal POINTS_LEVEL_4 = new BigDecimal("40");
	private static BigDecimal POINTS_LEVEL_5 = new BigDecimal("50");
	static
	    {
	       	levelPointMap.put(LEVEL_1, POINTS_LEVEL_1);
	       	levelPointMap.put(LEVEL_2, POINTS_LEVEL_2);
	       	levelPointMap.put(LEVEL_3, POINTS_LEVEL_3);
	       	levelPointMap.put(LEVEL_4, POINTS_LEVEL_4);
	       	levelPointMap.put(LEVEL_5, POINTS_LEVEL_5);
	    }
	
	private static final BigDecimal PERCENT_20 = new BigDecimal("0.2");
	private static final BigDecimal PERCENT_25 = new BigDecimal("0.25");
	private static final BigDecimal PERCENT_30 = new BigDecimal("0.3");
	private static final BigDecimal PERCENT_40 = new BigDecimal("0.4");
	
	public static Integer calculateCardPoints(CardInfo cardInfo, Player player){
		
		BigDecimal basicPoints = levelPointMap.get(cardInfo.getLevel());
		LOGGER.info("Basic points: {}",basicPoints);
		BigDecimal totalPoints = basicPoints;
		
		if(cardInfo.getDoneDate().after(cardInfo.getDueDate())){
			LOGGER.info("Tarea vencida - resta: {} puntos", basicPoints.multiply(PERCENT_40));
			totalPoints = totalPoints.subtract(basicPoints.multiply(PERCENT_40));
		}else{
			
			LOGGER.info("Tarea puntual - suma: {} puntos", basicPoints.multiply(PERCENT_20));
			totalPoints = totalPoints.add(basicPoints.multiply(PERCENT_20));
		}
		
		if(!cardInfo.getRole().equals(player.getRole())){
			LOGGER.info("Tarea de otro rol - suma: {} puntos", basicPoints.multiply(PERCENT_25));
			totalPoints = totalPoints.add(basicPoints.multiply(PERCENT_25));
		}
		
		if(cardInfo.isDocumented()){
			LOGGER.info("Tarea de documentada - suma: {} puntos", basicPoints.multiply(PERCENT_20));
			totalPoints = totalPoints.add(basicPoints.multiply(PERCENT_20));
		}
		LOGGER.info("Total points: {}",totalPoints);
		return totalPoints.intValue();
	}
	
	public static Integer calculateHelperPoints(CardInfo cardInfo){
		BigDecimal points = new BigDecimal(cardInfo.getPoints());
		return points.multiply(PERCENT_30).intValue();
	}

}