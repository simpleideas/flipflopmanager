package com.flipFlopManager.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.flipFlopManager.model.CardInfo;
import com.flipFlopManager.model.Player;

public interface CardsRepository extends MongoRepository<CardInfo, String>  {
	
	List<CardInfo> findAllByPlayerId(String playerId);

	List<CardInfo> findAllByPlayerIdAndWeekNumber(String googleEmail,
			Integer weekNumber);

}
