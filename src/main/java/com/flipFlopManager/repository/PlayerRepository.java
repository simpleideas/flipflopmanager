package com.flipFlopManager.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.flipFlopManager.model.Player;

public interface PlayerRepository extends MongoRepository<Player, String>  {
	
	Player findOneByAccessToken(String accessToken);
	
	Player findOneByGoogleEmail(String googleEmail);
	
	List<Player> findAllByBoardId(String boardId);

}
