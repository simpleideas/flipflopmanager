package com.flipFlopManager.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.flipFlopManager.model.CardInfo;
import com.flipFlopManager.model.Game;
import com.flipFlopManager.model.Player;

public interface GamesRepository extends MongoRepository<Game, String>  {

}
