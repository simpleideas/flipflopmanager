package com.flipFlopManager.repository;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.flipFlopManager.model.CardInfo;
import com.flipFlopManager.model.PlayerStats;

@Repository
public class CardsDao {
	
    @Autowired
    MongoTemplate mongoTemplate;
    
    
    public PlayerStats getPlayerStats(String playerId){
    	 List<AggregationOperation> ops = new ArrayList<AggregationOperation>();
    	 ops.add(match(Criteria.where("playerId").is(playerId)));
    	 ops.add(group("playerId").sum("points").as("points").count().as("cards"));
    	 
    	 Aggregation agg = newAggregation(ops.toArray(new AggregationOperation[] {}));
         AggregationResults<PlayerStats> groupResults = this.mongoTemplate.aggregate(agg, CardInfo.class, PlayerStats.class);
         List<PlayerStats> result = groupResults.getMappedResults();
         return result.get(0);
    	
    }

}
