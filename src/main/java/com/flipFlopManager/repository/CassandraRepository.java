package com.flipFlopManager.repository;

import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Repository;

import com.flipFlopManager.model.Player;
import com.google.common.collect.ImmutableMap;
import com.netflix.astyanax.AstyanaxContext;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.MutationBatch;
import com.netflix.astyanax.connectionpool.NodeDiscoveryType;
import com.netflix.astyanax.connectionpool.OperationResult;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.connectionpool.impl.ConnectionPoolConfigurationImpl;
import com.netflix.astyanax.connectionpool.impl.CountingConnectionPoolMonitor;
import com.netflix.astyanax.ddl.ColumnFamilyDefinition;
import com.netflix.astyanax.ddl.SchemaChangeResult;
import com.netflix.astyanax.impl.AstyanaxConfigurationImpl;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.serializers.IntegerSerializer;
import com.netflix.astyanax.serializers.StringSerializer;
import com.netflix.astyanax.thrift.ThriftFamilyFactory;


/**
 * @author Franco Vannasaeng on 6/28/15 6:40 PM
 */
//@Repository
public class CassandraRepository implements InitializingBean {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CassandraRepository.class);
	private static final String NO_OP_STRING = "no-op";
	private static final String CLUSTER_NAME = "Test Cluster";
	private static final String GAME_DATA_KEYSPACE_NAME = "GameData";
	private AstyanaxContext<Keyspace> context;
	
	private Keyspace keyspace;
	private ColumnFamily<Integer, String> playersCF;
	private static final String PLAYERS_CF_NAME = "players";
	
	@Override
	public void afterPropertiesSet() throws Exception {
		LOGGER.info("Starting Cassandra repository");
		
		context = new AstyanaxContext.Builder().forCluster(CLUSTER_NAME).forKeyspace(GAME_DATA_KEYSPACE_NAME)
				.withAstyanaxConfiguration(new AstyanaxConfigurationImpl().setDiscoveryType(NodeDiscoveryType.RING_DESCRIBE))
				.withConnectionPoolConfiguration(new ConnectionPoolConfigurationImpl("MyConnectionPool").setPort(9160).setMaxConnsPerHost(1).setSeeds("127.0.0.1:9160"))
				.withAstyanaxConfiguration(new AstyanaxConfigurationImpl().setCqlVersion("3.0.0"))
				// .setTargetCassandraVersion("1.2"))
				.withConnectionPoolMonitor(new CountingConnectionPoolMonitor()).buildKeyspace(ThriftFamilyFactory.getInstance());
				
		context.start();
		keyspace = context.getClient();
		
		// Create keyspace if not exist - Using simple strategy
		try {
			LOGGER.info("Creating the keyspace {} in the cluster {} if not exist", GAME_DATA_KEYSPACE_NAME,CLUSTER_NAME);
			OperationResult<SchemaChangeResult> keyspaceIfNotExists = keyspace.createKeyspaceIfNotExists(ImmutableMap.<String, Object> builder()
					.put("strategy_options", ImmutableMap.<String, Object> builder().put("replication_factor", "1").build()).put("strategy_class", "SimpleStrategy").build());
					
			LOGGER.debug("Result Object -> {}", keyspaceIfNotExists);
			LOGGER.debug("Result Object Result -> {}", keyspaceIfNotExists.getResult());
			LOGGER.debug("Result Object Result Schema Id -> {}", keyspaceIfNotExists.getResult().getSchemaId());
			
			if (NO_OP_STRING.equalsIgnoreCase(keyspaceIfNotExists.getResult().getSchemaId())) {
				LOGGER.info("The Keyspace already exist");
			}
			
		} catch (ConnectionException e) {
			LOGGER.error(e.getMessage());
			e.printStackTrace();
		}
		
		// EXIST playersCF
		try {
			LOGGER.info("Creating the CFs if not exist");
			ColumnFamilyDefinition playersCFDefinition = context.getClient().describeKeyspace().getColumnFamily(PLAYERS_CF_NAME);
			LOGGER.debug("playersCF -> {}", playersCFDefinition);
			
			if (playersCFDefinition == null) {
				playersCF = ColumnFamily.newColumnFamily(PLAYERS_CF_NAME, IntegerSerializer.get(), StringSerializer.get());
				LOGGER.info("Creating the CF {}", playersCF);
				keyspace.createColumnFamily(playersCF,
						ImmutableMap.<String, Object> builder()
								.put("column_metadata", ImmutableMap.<String, Object> builder()
										.put("name", ImmutableMap.<String, Object> builder().put("validation_class", "UTF8Type").put("index_name", "name").put("index_type", "KEYS").build())
										.put("email", ImmutableMap.<String, Object> builder().put("validation_class", "UTF8Type").put("index_name", "email").put("index_type", "KEYS").build())
										.put("accessToken", ImmutableMap.<String, Object> builder().put("validation_class", "UTF8Type").put("index_name", "accessToken").put("index_type", "KEYS").build())
										.put("idTrello", ImmutableMap.<String, Object> builder().put("validation_class", "UTF8Type").put("index_name", "idTrello").put("index_type", "KEYS").build()).build())
						.build());
			}
			
		} catch (ConnectionException e) {
			LOGGER.error(e.getMessage());
			e.printStackTrace();
		}
		
		LOGGER.info("Finishing Cassandra repository bean creation");
		
	}
	
	public void savePlayer(Player player) {
		LOGGER.info("Saving player {}", player);
		MutationBatch m = keyspace.prepareMutationBatch();


		playersCF = ColumnFamily.newColumnFamily(PLAYERS_CF_NAME, IntegerSerializer.get(), StringSerializer.get());
//		m.withRow(playersCF, player.hashCode()).putColumn("name", player.getFirstName(), null).putColumn("email", player.getEmail(), null).putColumn("accessToken", player.getAccessToken()).putColumn("idTrello", player.getIdTrello());

		try {
			OperationResult<Void> result = m.execute();
			LOGGER.debug("Result: {}", result);
			LOGGER.debug("Result.getResult: {}", result.getResult());
			LOGGER.debug("Result.getAttemptsCount: {}", result.getAttemptsCount());
		} catch (ConnectionException e) {
			LOGGER.error(e.getMessage());
			// throw new RuntimeException("Error trying to do a put in cache - key: " + key + " - column to insert: " + column, e);
		}
	}
	
	/*
	 * public void insertBatchMutation() { MutationBatch m = keyspace.prepareMutationBatch(); long rowKey = 1234; String key = "thisIsTheKey"; String key2 = "thisIsTheSecondKey"; // Setting columns in
	 * a standard column m.withRow(CF_STRING_KEY, key).putColumn("Column1", "X", null).putColumn("Column2", "X", null); m.withRow(CF_STRING_KEY, key2).putColumn("Column1", "Y", null); try {
	 * OperationResult<Void> result = m.execute(); LOGGER.info("Result: {}", result); } catch (ConnectionException e) { LOGGER.error(e.getMessage()); // throw new RuntimeException(
	 * "Error trying to do a put in cache - key: " + key + " - column to insert: " + column, e); } }
	 */
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.reflectionToString(this, RecursiveToStringStyle.SHORT_PREFIX_STYLE);
	}
	
}
