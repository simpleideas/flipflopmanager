package com.flipFlopManager.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.flipFlopManager.model.HelpRequest;
import com.flipFlopManager.model.HelpRequestStatus;

public interface HelpRequestRepository extends
		MongoRepository<HelpRequest, String> {

	HelpRequest findOneByCardId(String cardId);

	List<HelpRequest> findAllByHelperId(String helperId);
	
	List<HelpRequest> findAllByHelperIdAndWeekNumber(String helperId,Integer weekNumber);
	
	List<HelpRequest> findAllByBoardId(String boardId);

	List<HelpRequest> findAllByStatusAndBoardId(HelpRequestStatus status,
			String boardId);

	@Query(value = "{'$and' :[{'status' : {$equals : ?0},{'requesterId':{$ne : ?1}}]}")
	List<HelpRequest> findByFilters(HelpRequestStatus status, String requesterId);

}
