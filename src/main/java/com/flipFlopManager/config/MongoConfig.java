package com.flipFlopManager.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

@Configuration
@EnableMongoRepositories("com.flipFlopManager.repository")
public class MongoConfig extends AbstractMongoConfiguration {

	@Override
	protected String getDatabaseName() {
		return "flip-flop";
	}

	@Override
	public Mongo mongo() throws Exception {
		String textUri = "mongodb://admin:admin@ds045684.mongolab.com:45684/flip-flop";
		MongoClientURI uri = new MongoClientURI(textUri);
		return new MongoClient(uri);
	}

	
}
