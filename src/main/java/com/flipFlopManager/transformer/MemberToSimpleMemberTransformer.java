package com.flipFlopManager.transformer;

import org.springframework.stereotype.Component;

import com.flipFlopManager.model.SimpleMember;
import com.google.common.base.Function;
import com.julienvey.trello.domain.Member;

@Component
public class MemberToSimpleMemberTransformer implements Function<Member, SimpleMember> {

	@Override
	public SimpleMember apply(Member member) {
		SimpleMember simpleMember = new SimpleMember();
		simpleMember.setAvatarHash(member.getAvatarHash());
		simpleMember.setAvatarSource(member.getAvatarSource());
		simpleMember.setBio(member.getBio());
		simpleMember.setFullName(member.getFullName());
		simpleMember.setIdTrello(member.getId());
		simpleMember.setInitials(member.getInitials());
		simpleMember.setMemberType(member.getMemberType());
		simpleMember.setStatus(member.getStatus());
		simpleMember.setUsername(member.getUsername());
		simpleMember.setEmail(member.getEmail());
		simpleMember.setGravatarHash(member.getGravatarHash());
		simpleMember.setUrl(member.getUrl());
		return simpleMember;
	}

}
