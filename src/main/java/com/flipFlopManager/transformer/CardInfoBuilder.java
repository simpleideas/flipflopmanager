package com.flipFlopManager.transformer;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.flipFlopManager.model.CardInfo;
import com.flipFlopManager.model.Constants;
import com.flipFlopManager.model.HelpRequest;
import com.flipFlopManager.model.Player;
import com.flipFlopManager.model.Role;
import com.flipFlopManager.repository.HelpRequestRepository;
import com.flipFlopManager.service.Calculator;
import com.julienvey.trello.Trello;
import com.julienvey.trello.domain.Action;
import com.julienvey.trello.domain.Attachment;
import com.julienvey.trello.domain.Card;
import com.julienvey.trello.domain.Label;
import com.julienvey.trello.domain.TList;

@Component
public class CardInfoBuilder {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CardInfoBuilder.class);
	@Autowired
	private Trello trello;
	@Autowired 
	private HelpRequestRepository helpRequestRepository;
	
	public CardInfo buildCardInfo(Card card, Player player, int weekNumber){
		Map<String, String> cardLabels = getCardLabels(card);
		CardInfo cardInfo = new CardInfo();
		cardInfo.setCardId(card.getId());
		cardInfo.setBoardId(player.getBoardId());
		cardInfo.setPlayerId(player.getGoogleEmail());
		cardInfo.setDocumented(isDocAttachment(card, player));
		List<Action> cardActions = this.trello.getCardActions(card.getId() ,player.getAccessToken());
		cardInfo.setDoneDate(getDoneDate(cardActions, player));
		cardInfo.setInProgressDate(getInProgressDate(cardActions, player));
		cardInfo.setDueDate(card.getDue());
		cardInfo.setName(card.getName());
		cardInfo.setRole(Role.valueOf(cardLabels.get(Constants.ROLE)));
		cardInfo.setLevel(cardLabels.get(Constants.LEVEL));
		cardInfo.setPoints(Calculator.calculateCardPoints(cardInfo, player));
		cardInfo.setSolvedOnTime(cardInfo.getDoneDate().after(cardInfo.getDueDate()));
		cardInfo.setSolvedWithHelp(card.getIdMembers().size() > 1);
		cardInfo.setWeekNumber(weekNumber);
		if(cardInfo.isSolvedWithHelp()){
			this.resolveHelpRequest(cardInfo);
		}
		LOGGER.info("CardInfo created: {}",cardInfo);
		return cardInfo;
	}
	
	private void resolveHelpRequest(CardInfo cardInfo) {
		HelpRequest helpRequest = this.helpRequestRepository.findOneByCardId(cardInfo.getCardId());
		if(helpRequest != null){
			helpRequest.setWeekNumber(cardInfo.getWeekNumber());
			helpRequest.setPoints(Calculator.calculateHelperPoints(cardInfo));
			this.helpRequestRepository.save(helpRequest);
		}
	}

	private Date getDoneDate(List<Action> cardActions, Player player) {
		for (Action action : cardActions) {
			TList listAfter = action.getData().getListAfter();
			if(listAfter != null){
				String listAfterName = listAfter.getName();
				if(Constants.DONE.equalsIgnoreCase(listAfterName)){
					return action.getDate();
				}
			}
		}
		return null;
	}
	
	private Date getInProgressDate(List<Action> cardActions, Player player) {
		for (Action action : cardActions) {
			TList listAfter = action.getData().getListAfter();
			if(listAfter != null){
				String listAfterName = listAfter.getName();
				if(Constants.IN_PROGRESS.equalsIgnoreCase(listAfterName)){
					return action.getDate();
				}
			}
		}
		return null;
	}
	
	private boolean isDocAttachment(Card card,Player player){
		LOGGER.info("Getting card attachments...");
		List<Attachment> cardAttachments = this.trello.getCardAttachments(card.getId(), player.getAccessToken());
		for (Attachment attachment : cardAttachments) {
			if(attachment.getName().startsWith(Constants.DOC_PREFIX)){
				LOGGER.info("The card contains documentation: {}", attachment.getName());
				return true;
			}
		}
		LOGGER.info("The card does not have documentation!");
		return false;
	}
	
	private Map<String,String> getCardLabels(Card card){
		Map<String,String> labels  = new HashMap<String,String>();
		for (Label label : card.getLabels()) {
			switch (label.getColor()) {
			case Constants.LEVEL_COLOR:
				labels.put(Constants.LEVEL, label.getName());
				break;
			case Constants.ROLE_COLOR:
				labels.put(Constants.ROLE, label.getName());
				break;	
			case Constants.ISO_COLOR:
				labels.put(Constants.ISO, label.getName());
				break;	
			default:
				break;
			}
		}
		return labels;
	}
}
