package com.flipFlopManager.transformer;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.flipFlopManager.model.HelpRequest;
import com.flipFlopManager.model.Player;
import com.flipFlopManager.repository.PlayerRepository;
import com.flipFlopManager.rest.response.HelpRequestCardView;
import com.flipFlopManager.service.PlayerBuilder;
import com.google.common.base.Function;
import com.julienvey.trello.domain.Card;

@Component
public class HelpRequestToCardViewTransformer{

	@Autowired
	private PlayerBuilder playerBuilder;
	@Autowired
	private PlayerRepository playerRepository;

	
	public HelpRequestCardView apply(HelpRequest input, Player playerProfile) {

		Player requester = this.playerRepository.findOneByGoogleEmail(input
				.getRequesterId());
		this.playerBuilder.addInfoToPlayer(requester);

		Card card = getCard(requester, input);

		HelpRequestCardView helpRequestCardView = new HelpRequestCardView();
		helpRequestCardView.setAvatarUrl(requester.getMember().getAvatarHash());
		helpRequestCardView.setCardId(card.getId());
		helpRequestCardView.setDescription(card.getName());
		helpRequestCardView.setSinceHours(getSinceHours(input));
		helpRequestCardView.setUsername(requester.getMember().getUsername());
		helpRequestCardView.setOwner(requester.getGoogleEmail().equals(playerProfile.getGoogleEmail()));
		return helpRequestCardView;
	}

	private String getSinceHours(HelpRequest input) {
		DateTime creationDate = new DateTime(input.getCreationDate());
		Period p = new Period(creationDate, DateTime.now());

		if (p.getDays() == 1) {
			return String.valueOf(p.getDays()).concat(" día");
		} else if (p.getDays() > 1) {
			return String.valueOf(p.getDays()).concat(" días");
		} else if (p.getHours() == 0) {
			return String.valueOf(p.getMinutes()).concat(" min");
		} else {
			return String.valueOf(p.getHours()).concat(" hr");
		}
	}

	private Card getCard(Player requester, HelpRequest input) {
		for (Card card : requester.getInProgressTasks()) {
			if (card.getId().equals(input.getCardId())) {
				return card;
			}
		}
		return null;
	}

}