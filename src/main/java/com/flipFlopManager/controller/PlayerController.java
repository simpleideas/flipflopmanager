package com.flipFlopManager.controller;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.flipFlopManager.model.Player;
import com.flipFlopManager.model.PlayerStats;
import com.flipFlopManager.rest.request.PlayerDTO;
import com.flipFlopManager.rest.response.AccessTokenResponse;
import com.flipFlopManager.service.PlayerService;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.julienvey.trello.domain.Board;
import com.julienvey.trello.domain.Card;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/player")
@Api(value="player", description = "Endpoint for player management")
public class PlayerController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(PlayerController.class);
	private PlayerService playerService;

	@Autowired
	public PlayerController(PlayerService playerService) {
		this.playerService = playerService;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "")
	@ApiOperation(value = "Create a new player and return it ", response = Player.class)
	public Player createNewPlayer(@RequestBody PlayerDTO playerDTO) {
		return this.playerService.create(playerDTO);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "{token}/boardId")
	public Player updateBoardId(@PathVariable String token, @RequestBody PlayerDTO playerDTO){
		return this.playerService.updateBoardId(token,playerDTO.getBoardId());
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "{token}/gcmRegistrationToken")
	public Player updateGcmRegistrationToken(@PathVariable String token, @RequestBody PlayerDTO playerDTO){
		return this.playerService.updateGcmRegistrationToken(token,playerDTO.getGcmRegistrationToken());
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "{token}/role")
	public Player updateRole(@PathVariable String token, @RequestBody PlayerDTO playerDTO){
		return this.playerService.updateRole(token, playerDTO.getRole());
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "{token}")
	public Player updateRoleAndBoard(@PathVariable String token, @RequestBody PlayerDTO playerDTO){
		return this.playerService.updateRoleAndBoard(token, playerDTO);
	}
	
	//no se usa, solo para probar el jsonPatch
	@RequestMapping(method = RequestMethod.PATCH, value = "{token}",headers = {"content-type=application/json-patch+json"})
	public Player updatePlayer(@PathVariable String token, @RequestBody JsonPatch jsonPatch) throws IOException, JsonPatchException {
		return this.playerService.update(token,jsonPatch);
	}

	@RequestMapping(method = RequestMethod.GET, value ="{token}")
	public Player getPlayer(@PathVariable String token) {
		LOGGER.info("token parameter -> {}", token);
		return this.playerService.getPlayerByToken(token);
	}
	
	@RequestMapping(method = RequestMethod.GET, value ="{googleEmail}/token")
	public ResponseEntity<AccessTokenResponse> getPlayerToken(@PathVariable String googleEmail ) {
		LOGGER.info("google email parameter -> {}", googleEmail);
		AccessTokenResponse response = this.playerService.getPlayerToken(googleEmail);
		if (response != null){
			return new ResponseEntity<AccessTokenResponse>(response, HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(method = RequestMethod.GET, value ="{token}/boards")
	public List<Board> getPlayerBoards(@PathVariable String token) {
		LOGGER.info("token parameter -> {}", token);
		return this.playerService.getBoards(token);
	}

	@RequestMapping(method = RequestMethod.GET, value ="{token}/cards")
	public List<Card> getPlayerCards(@PathVariable String token) {
		return this.playerService.getCards(token);
	}
	
	@RequestMapping(method = RequestMethod.GET, value ="{token}/lastStats")
	public PlayerStats getLastPlayerStats(@PathVariable String token) {
		return this.playerService.getLastPlayerStats(token);
	}
	
	
	
}