package com.flipFlopManager.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.flipFlopManager.model.Player;
import com.flipFlopManager.service.PlayerService;
import com.wordnik.swagger.annotations.Api;

@RestController
@RequestMapping("/private")
@Api(value="private", description = "Endpoint for private operations for development")
public class PrivateController {
	
	private PlayerService playerService;

	@Autowired
	public PrivateController(PlayerService playerService) {
		this.playerService = playerService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value ="/players")
	public List<Player> getPlayers() {
		return this.playerService.getPlayers();
	}

}
