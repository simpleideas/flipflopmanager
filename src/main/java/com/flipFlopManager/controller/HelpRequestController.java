package com.flipFlopManager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.flipFlopManager.model.HelpRequest;
import com.flipFlopManager.rest.request.HelpRequestDTO;
import com.flipFlopManager.service.HelpRequestService;
import com.wordnik.swagger.annotations.Api;

@RestController
@RequestMapping("/help-requests")
@Api(value="help-requests", description = "Endpoint for help-requests management")
public class HelpRequestController {

	private HelpRequestService helpRequestService;

	@Autowired
	public HelpRequestController(HelpRequestService helpRequestService) {
		this.helpRequestService = helpRequestService;
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/open")
	public HelpRequest openHelpRequest(@RequestBody HelpRequestDTO helpRequestDTO) {
		return this.helpRequestService.openHelpRequest(helpRequestDTO);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/close")
	public HelpRequest closeHelpRequest(@RequestBody HelpRequestDTO helpRequestDTO) {
		return this.helpRequestService.closeHelpRequest(helpRequestDTO);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/{cardId}")
	public ResponseEntity<HelpRequest> deleteHelpRequest(@PathVariable String cardId) {
		HelpRequest deleted = this.helpRequestService.deleteHelpRequest(cardId);
		if(deleted != null){
			return new ResponseEntity<HelpRequest>(deleted,HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
}
