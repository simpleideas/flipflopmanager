package com.flipFlopManager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flipFlopManager.service.AwardsCalculatorService;

@RestController
@RequestMapping("/award")
public class AwardController {

	private AwardsCalculatorService awardsCalculatorService;
	
	@Autowired
	public AwardController(AwardsCalculatorService awardsCalculatorService){
		this.awardsCalculatorService = awardsCalculatorService;
	}
	
	// solo para correr manualmente la entrega de puntos
	@RequestMapping("/test")
	public void testAward(){
		this.awardsCalculatorService.calculatePoints();
	}
	
}
