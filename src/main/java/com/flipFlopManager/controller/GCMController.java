package com.flipFlopManager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.flipFlopManager.model.PlayerStats;
import com.flipFlopManager.rest.response.GCMResponse;
import com.flipFlopManager.service.GCMService;

@RestController
@RequestMapping("/message")
public class GCMController {

	private GCMService gcmService;

	@Autowired
	public GCMController(GCMService gcmService) {
		this.gcmService = gcmService;
	}

	@RequestMapping("/test/{message}")
	public GCMResponse sendMessage(@PathVariable String message) {
		return this.gcmService.sendMessage(message);
	}
	
	@RequestMapping(value = "/send/{gcmToken}", method = RequestMethod.POST)
	public GCMResponse sendMessageWithBody(@PathVariable String gcmToken, @RequestBody PlayerStats playerStats) {
		return this.gcmService.sendMessage("test mock", gcmToken,playerStats);
	}
}
