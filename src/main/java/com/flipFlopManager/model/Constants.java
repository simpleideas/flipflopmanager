package com.flipFlopManager.model;

public final class Constants {

	// list names
	public static final String TO_DO = "POR HACER";
	public static final String IN_PROGRESS = "EN PROGRESO";
	public static final String DONE = "TERMINADO";

	// documentation prefix
	public static final String DOC_PREFIX = "DOC:";

	// label colors
	public static final String LEVEL_COLOR = "green";
	public static final String ROLE_COLOR = "yellow";
	public static final String ISO_COLOR = "red";

	// label names 
	public static final String LEVEL = "NIVEL";
	public static final String ROLE = "ROL";
	public static final String ISO = "ISO";
	
	// gcm messages
	public static final String POINTS_CALCULATION_MESSAGE_GENERIC = "¡Entrega de premios finalizada. Descubrí tu nueva puntuación!";
	public static final String POINTS_CALCULATION_MESSAGE_WINNER = "¡Felicitaciones, sos el ganador de la semana. Descubrí tu nueva puntuación!";
	public static final String POINTS_CALCULATION_MESSAGE_LOSER = "¡Malas noticias, sos el perdedor de la semana. Descubrí tu nueva puntuación!";
}
