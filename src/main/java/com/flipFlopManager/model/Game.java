package com.flipFlopManager.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.julienvey.trello.domain.Card;
import com.julienvey.trello.domain.Member;

@Document(collection = "games")
public class Game implements Serializable {

	@Id
	private String boardId;
	private String todoListId;
	private String inProgressListId;
	private String doneListId;
	private int weekNumber = 0;

	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

	public String getTodoListId() {
		return todoListId;
	}

	public void setTodoListId(String todoListId) {
		this.todoListId = todoListId;
	}

	public String getInProgressListId() {
		return inProgressListId;
	}

	public void setInProgressListId(String inProgressListId) {
		this.inProgressListId = inProgressListId;
	}

	public String getDoneListId() {
		return doneListId;
	}

	public void setDoneListId(String doneListId) {
		this.doneListId = doneListId;
	}

	public int getWeekNumber() {
		return weekNumber;
	}

	public void setWeekNumber(int weekNumber) {
		this.weekNumber = weekNumber;
	}

	@Override
	public String toString() {
		return "Game [boardId=" + boardId + ", todoListId=" + todoListId
				+ ", inProgressListId=" + inProgressListId + ", doneListId="
				+ doneListId + ", weekNumber=" + weekNumber + "]";
	}

}