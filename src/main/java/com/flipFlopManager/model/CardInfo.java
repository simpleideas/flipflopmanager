package com.flipFlopManager.model;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "cards")
public class CardInfo {

	private String id;
	private String cardId;
	private String boardId;
	private String playerId;
	private String name;
	private Integer points;
	private String level;
	private Role role;
	private Date dueDate;
	private Date doneDate;
	private Date inProgressDate;
	private boolean documented;
	private boolean solvedWithHelp;
	private boolean solvedOnTime;
	private int weekNumber;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getDoneDate() {
		return doneDate;
	}

	public void setDoneDate(Date doneDate) {
		this.doneDate = doneDate;
	}

	public boolean isDocumented() {
		return documented;
	}

	public void setDocumented(boolean documented) {
		this.documented = documented;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public boolean isSolvedWithHelp() {
		return solvedWithHelp;
	}

	public void setSolvedWithHelp(boolean solvedWithHelp) {
		this.solvedWithHelp = solvedWithHelp;
	}

	public Date getInProgressDate() {
		return inProgressDate;
	}

	public void setInProgressDate(Date inProgressDate) {
		this.inProgressDate = inProgressDate;
	}

	public boolean isSolvedOnTime() {
		return solvedOnTime;
	}

	public void setSolvedOnTime(boolean solvedOnTime) {
		this.solvedOnTime = solvedOnTime;
	}

	public int getWeekNumber() {
		return weekNumber;
	}

	public void setWeekNumber(int weekNumber) {
		this.weekNumber = weekNumber;
	}

	@Override
	public String toString() {
		return "CardInfo [id=" + id + ", cardId=" + cardId + ", boardId="
				+ boardId + ", playerId=" + playerId + ", name=" + name
				+ ", points=" + points + ", level=" + level + ", role=" + role
				+ ", dueDate=" + dueDate + ", doneDate=" + doneDate
				+ ", inProgressDate=" + inProgressDate + ", documented="
				+ documented + ", solvedWithHelp=" + solvedWithHelp
				+ ", solvedOnTime=" + solvedOnTime + ", weekNumber="
				+ weekNumber + "]";
	}

}