package com.flipFlopManager.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.mongodb.core.mapping.Document;

import com.flipFlopManager.rest.response.HelpRequestCardView;
import com.julienvey.trello.domain.Card;

/**
 * @author Franco Vannasaeng on 6/24/15 10:15 PM
 */
@Document(collection = "player")
public class Player implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private String accessToken;
	private String boardId;
	private String trelloId;
	private String googleEmail;
	private String gcmRegistrationToken;
	private Role role;
	private SimpleMember member;
	private int availableHelpRequests = 0;
	private int initialHelpRequests = 3;
	private int documentedCardsCounter = 0;
	private int totalGamesWon = 0;
	private Boolean helpRequestEnabled;

	private List<Card> todoTasks;
	private List<Card> inProgressTasks;
	private List<Card> doneTasks;
	private List<Card> allTasks;
	private List<HelpRequestCardView> helpRequests;

	private PlayerStats stats;
	private List<Skill> skills;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<Card> getTodoTasks() {
		if (todoTasks == null) {
			todoTasks = new ArrayList<Card>();
		}
		return todoTasks;
	}

	public void setTodoTasks(List<Card> todoTasks) {
		this.todoTasks = todoTasks;
	}

	public List<Card> getInProgressTasks() {
		if (inProgressTasks == null) {
			inProgressTasks = new ArrayList<Card>();
		}
		return inProgressTasks;
	}

	public void setInProgressTasks(List<Card> inProgressTasks) {
		this.inProgressTasks = inProgressTasks;
	}

	public List<Card> getDoneTasks() {
		if (doneTasks == null) {
			doneTasks = new ArrayList<Card>();
		}
		return doneTasks;
	}

	public void setDoneTasks(List<Card> doneTasks) {
		this.doneTasks = doneTasks;
	}

	public List<Card> getAllTasks() {
		if (allTasks == null) {
			allTasks = new ArrayList<Card>();
		}
		return allTasks;
	}

	public void setAllTasks(List<Card> allTasks) {
		this.allTasks = allTasks;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

	public String getGoogleEmail() {
		return googleEmail;
	}

	public void setGoogleEmail(String googleEmail) {
		this.googleEmail = googleEmail;
	}

	public SimpleMember getMember() {
		return member;
	}

	public void setMember(SimpleMember member) {
		this.member = member;
	}

	public String getGcmRegistrationToken() {
		return gcmRegistrationToken;
	}

	public void setGcmRegistrationToken(String gcmRegistrationToken) {
		this.gcmRegistrationToken = gcmRegistrationToken;
	}

	public String getTrelloId() {
		return trelloId;
	}

	public void setTrelloId(String trelloId) {
		this.trelloId = trelloId;
	}

	public PlayerStats getStats() {
		return stats;
	}

	public void setStats(PlayerStats stats) {
		this.stats = stats;
	}

	public List<Skill> getSkills() {
		if (this.skills == null) {
			this.skills = new ArrayList<Skill>();
		}
		return skills;
	}

	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}

	public List<HelpRequestCardView> getHelpRequests() {
		if (this.helpRequests == null) {
			this.helpRequests = new ArrayList<HelpRequestCardView>();
		}
		return helpRequests;
	}

	public void setHelpRequests(List<HelpRequestCardView> helpRequests) {
		this.helpRequests = helpRequests;
	}

	public int getAvailableHelpRequests() {
		return availableHelpRequests;
	}

	public void setAvailableHelpRequests(int availableHelpRequests) {
		this.availableHelpRequests = availableHelpRequests;
	}

	public int getTotalGamesWon() {
		return totalGamesWon;
	}

	public void setTotalGamesWon(int totalGamesWon) {
		this.totalGamesWon = totalGamesWon;
	}

	public int getDocumentedCardsCounter() {
		return documentedCardsCounter;
	}

	public void setDocumentedCardsCounter(int documentedCardsCounter) {
		this.documentedCardsCounter = documentedCardsCounter;
	}

	public Boolean getHelpRequestEnabled() {
		return helpRequestEnabled;
	}

	public void setHelpRequestEnabled(Boolean helpRequestEnabled) {
		this.helpRequestEnabled = helpRequestEnabled;
	}

	public int getInitialHelpRequests() {
		return initialHelpRequests;
	}

	public void setInitialHelpRequests(int initialHelpRequests) {
		this.initialHelpRequests = initialHelpRequests;
	}
	
	

}