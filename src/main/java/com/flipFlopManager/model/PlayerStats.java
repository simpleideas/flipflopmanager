package com.flipFlopManager.model;

public class PlayerStats {

	private int totalPoints = 0;
	private int totalCards = 0;
	private int totalDocumented = 0;
	private int totalOtherRoles = 0;
	private int totalOnTime = 0;
	private int totalWithoutHelp = 0;
	private int totalWithHelp = 0;
	private int sumLevel = 0;
	private int totalHelper = 0;
	private int totalGamesWon = 0;

	public int getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(int totalPoints) {
		this.totalPoints = totalPoints;
	}

	public int getTotalCards() {
		return totalCards;
	}

	public void setTotalCards(int totalCards) {
		this.totalCards = totalCards;
	}

	public int getTotalDocumented() {
		return totalDocumented;
	}

	public void setTotalDocumented(int totalDocumented) {
		this.totalDocumented = totalDocumented;
	}

	public int getTotalOtherRoles() {
		return totalOtherRoles;
	}

	public void setTotalOtherRoles(int totalOtherRoles) {
		this.totalOtherRoles = totalOtherRoles;
	}

	public int getTotalOnTime() {
		return totalOnTime;
	}

	public void setTotalOnTime(int totalOnTime) {
		this.totalOnTime = totalOnTime;
	}

	public int getTotalWithoutHelp() {
		return totalWithoutHelp;
	}

	public void setTotalWithoutHelp(int totalWithoutHelp) {
		this.totalWithoutHelp = totalWithoutHelp;
	}

	public int getTotalWithHelp() {
		return totalWithHelp;
	}

	public void setTotalWithHelp(int totalWithHelp) {
		this.totalWithHelp = totalWithHelp;
	}

	public int getSumLevel() {
		return sumLevel;
	}

	public void setSumLevel(int sumLevel) {
		this.sumLevel = sumLevel;
	}

	public int getTotalHelper() {
		return totalHelper;
	}

	public void setTotalHelper(int totalHelper) {
		this.totalHelper = totalHelper;
	}

	public int getTotalGamesWon() {
		return totalGamesWon;
	}

	public void setTotalGamesWon(int totalGamesWon) {
		this.totalGamesWon = totalGamesWon;
	}
	
	
	
}