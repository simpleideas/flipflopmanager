package com.flipFlopManager.model;

import java.util.List;

public class SimpleMember {
	private String idTrello;
	private String username;
	private String fullName;
	private String avatarHash;
	private String avatarSource;
	private String bio;
	private String initials;
	private String memberType;
	private String status;
	private String url;
	private String email;
	private String gravatarHash;
	
	
	public String getIdTrello() {
		return idTrello;
	}
	public void setIdTrello(String idTrello) {
		this.idTrello = idTrello;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getAvatarHash() {
		return avatarHash;
	}
	public void setAvatarHash(String avatarHash) {
		this.avatarHash = avatarHash;
	}
	public String getAvatarSource() {
		return avatarSource;
	}
	public void setAvatarSource(String avatarSource) {
		this.avatarSource = avatarSource;
	}
	public String getBio() {
		return bio;
	}
	public void setBio(String bio) {
		this.bio = bio;
	}
	public String getInitials() {
		return initials;
	}
	public void setInitials(String initials) {
		this.initials = initials;
	}
	public String getMemberType() {
		return memberType;
	}
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGravatarHash() {
		return gravatarHash;
	}
	public void setGravatarHash(String gravatarHash) {
		this.gravatarHash = gravatarHash;
	}
	
	
}
