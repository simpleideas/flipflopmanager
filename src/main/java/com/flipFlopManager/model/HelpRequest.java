package com.flipFlopManager.model;

import java.util.Date;

import org.joda.time.DateTime;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "help_request")
public class HelpRequest {

	private String id;
	private String cardId;
	private String helperId;
	private String requesterId;
	private Date creationDate;
	private HelpRequestStatus status;
	private String boardId;
	private int weekNumber;
	private int points = 0;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getHelperId() {
		return helperId;
	}

	public void setHelperId(String helperId) {
		this.helperId = helperId;
	}

	public String getRequesterId() {
		return requesterId;
	}

	public void setRequesterId(String requesterId) {
		this.requesterId = requesterId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public HelpRequestStatus getStatus() {
		return status;
	}

	public void setStatus(HelpRequestStatus status) {
		this.status = status;
	}

	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

	public int getWeekNumber() {
		return weekNumber;
	}

	public void setWeekNumber(int weekNumber) {
		this.weekNumber = weekNumber;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	@Override
	public String toString() {
		return "HelpRequest [id=" + id + ", cardId=" + cardId + ", helperId="
				+ helperId + ", requesterId=" + requesterId + ", creationDate="
				+ creationDate + ", status=" + status + ", boardId=" + boardId
				+ ", weekNumber=" + weekNumber + ", points=" + points + "]";
	}

}