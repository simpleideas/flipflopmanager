package com.flipFlopManager.model;

public enum Role {
	AN("ANALISTA"),PR("PROGRAMADOR"),DES("DISEÑADOR"),TL("LIDER TECNICO");
	
	private String name;
	
	private Role(String name){
		this.setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String value) {
		this.name = value;
	}
}
