package com.flipFlopManager.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author Franco Vannasaeng
 *         on 6/24/15 11:09 PM
 */
public class Task  implements Serializable {


	// TENTATIVE
	private String name;
	private String desc;
	private boolean closed;
	private Long idShort;
	private String idList;
	private String idBoard;
	private List<String> idMembers;
	private String url;
	private double pos;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public boolean isClosed() {
		return closed;
	}

	public void setClosed(boolean closed) {
		this.closed = closed;
	}

	public Long getIdShort() {
		return idShort;
	}

	public void setIdShort(Long idShort) {
		this.idShort = idShort;
	}

	public String getIdList() {
		return idList;
	}

	public void setIdList(String idList) {
		this.idList = idList;
	}

	public String getIdBoard() {
		return idBoard;
	}

	public void setIdBoard(String idBoard) {
		this.idBoard = idBoard;
	}

	public List<String> getIdMembers() {
		return idMembers;
	}

	public void setIdMembers(List<String> idMembers) {
		this.idMembers = idMembers;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public double getPos() {
		return pos;
	}

	public void setPos(double pos) {
		this.pos = pos;
	}
}
