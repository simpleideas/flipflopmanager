package com.flipFlopManager.model;

public class Skill {

	private SkillName name;
	private int percentage;
	
	public Skill(SkillName name, int percentage) {
		super();
		this.name = name;
		this.percentage = percentage;
	}

	public SkillName getName() {
		return name;
	}

	public void setName(SkillName name) {
		this.name = name;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

}
