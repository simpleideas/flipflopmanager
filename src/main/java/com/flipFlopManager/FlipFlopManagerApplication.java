package com.flipFlopManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@ImportResource("classpath:application-context.xml")
@EnableScheduling
public class FlipFlopManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(FlipFlopManagerApplication.class, args);
    }
}
