package com.julienvey.trello.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class CardWithActions extends Card {
	
	private List<Action> actions;
	
	public List<Action> getActions() {
		return actions;
	}
	
}
